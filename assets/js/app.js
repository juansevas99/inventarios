const d = document

function display(response) {
	$cabecera = document.querySelector("#tabla-admin > .cabecera")
	$cadenaCabecera = ""
	for (let i = 0; i < Object.keys(response[0]).length; i++) {
		$cadenaCabecera += "<th>" + Object.keys(response[0])[i] + "</th>"
	}
	$cadenaCabecera += "<th colspan=3>Accciones</th>"
	$cabecera.innerHTML = $cadenaCabecera
	$cuerpo = document.querySelector("#tabla-admin > .cuerpo")
	$cadenaCuerpo = ""
	for (let j = 0; j < response.length; j++) {
		$cadenaCuerpo += "<tr>"
		for (let k = 0; k < Object.keys(response[j]).length; k++) {
			$cadenaCuerpo += "<td>" + response[j][Object.keys(response[0])[k]] + "</td>"
		}

		$cadenaCuerpo +=
			"<td><button type='button' class='btn btn-primary' data-bs-toggle='modal' style='font-size: 80%;' data-bs-target='#Detalles'>Detalles</button><td><button style='font-size: 80%;' type='button' class='btn btn-primary popUpdate' onclick='updateAdmin(this)' data-id='" +
			response[j][Object.keys(response[0])[0]] +
			"' >Actualizar</button></td><td><button style='font-size: 80%;' class='btn btn-danger' onclick=deleteAdmin(this) data-id=" +
			response[j][Object.keys(response[0])[0]] +
			">Borrar</button></td></tr>"
	}
	

	$cuerpo.innerHTML = $cadenaCuerpo
}
function administracion(element,id) {
	// document.getElementById('crearRegistro')
	console.log(element + "/list")
	fetch(URL+ element + "/list/"+id, {
		method: "GET",
	})
		.then((response) => {
			if (response.ok == false || response.status > 299) {
				return Promise.reject( "Error, no se encontro el archivo json" )
			}
			return response.json()
		})
		.then((response) => {
			console.log(response)
			
			display(response[0])
			if (response[1]){
				document.getElementById('pagination').classList.remove('d-none');
				paginate(response[1]);
			}
			else{
				document.getElementById('pagination').classList.add('d-none');
			}
		})
		.catch((err) => {
			console.error(err)
		})
}
function crearProducto() {
	// por ahora, la busqueda de datos en cada Select no dependera de la vista si no de la logica que se maneje en Javascript
	document
		.querySelector('[name="estado_id_estado"]')
		.addEventListener("focus", (el) => {
			fetch(URL+"state/list")
				.then((response) => {
					if (response.ok == false || response.status > 299) {
						return Promise.reject({ err: "Error, no se encontro el archivo" })
					}
					return response.json()
				})
				.then((response) => {
					options = ""

					for (let i = 0; i < response.length; i++) {
						options +=
							"<option value=" +
							response[i].id +
							">" +
							response[i].nombre +
							"</option>"
					}
					document.querySelector('[name="estado_id_estado"]').innerHTML = options
				})
				.catch((err) => {
					console.error("Error Servidor !!" + err.err)
				})
		})
	document
		.querySelector('[name="categoria_id_categoria"]')
		.addEventListener("focus", () => {
			fetch(URL+"category/list")
				.then((response) => {
					if (response.ok == false || response.status > 299) {
						return Promise.reject({ err: "Error, no se encontro el archivo" })
					}
					return response.json()
				})
				.then((response) => {
					options = ""
					// console.log(response);
					for (let i = 0; i < response[0].length; i++) {
						options +=
							"<option value=" +
							response[0][i].COD +
							">" +
							response[0][i].Categoria +
							"</option>"
					}

					document.querySelector('[name="categoria_id_categoria"]').innerHTML =
						options
				})

				.catch((err) => {
					console.error("Error Servidor !!" + err.err)
				})
		})
	document
		.querySelector('[name="proveedor_id_proveedor"]')
		.addEventListener("focus", () => {
			fetch(URL+"supplier/listInout")
				.then((response) => {
					if (response.ok == false || response.status > 299) {
						return Promise.reject({ err: "Error, no se encontro el archivo" })
					}
					return response.json()
				})
				.then((response) => {
					options = ""
					// console.log(response);
					for (let i = 0; i < response[0].length; i++) {
						options +=
							"<option value=" +
							response[0][i].Id +
							">" +
							response[0][i].Nombre +
							"</option>"
					}

					document.querySelector('[name="proveedor_id_proveedor"]').innerHTML =
						options
				})

				.catch((err) => {
					console.error("Error Servidor !!" + err.err)
				})
		})
	document
		.querySelector('[name="marca_id_marca"]')
		.addEventListener("click", () => {
			fetch(URL+"brand/list")
				.then((response) => {
					if (response.ok == false || response.status > 299) {
						return Promise.reject({ err: "Error, no se encontro el archivo" })
					}
					return response.json()
				})
				.then((response) => {
					options = ""
					// console.log(response[0])
					for (let i = 0; i < response[0].length; i++) {
						options +=
							"<option value=" +
							response[0][i].COD +
							">" +
							response[0][i].Marca +
							"</option>"
					}

					document.querySelector('[name="marca_id_marca"]').innerHTML = options
				})

				.catch((err) => {
					console.error("Error Servidor !!" + err.err)
				})
		})

		document
		.getElementById('atributos')
		.addEventListener("click", () => {
			fetch(URL+"atributoscat/list")
				.then((response) => {
					if (response.ok == false || response.status > 299) {
						return Promise.reject({ err: "Error, no se encontro el archivo" })
					}
					return response.json()
				})
				.then((response) => {
					atributes = ""
					categoria=document.getElementById('atributos').getAttribute('name');
					// console.log(response[0])
					for (let i = 0; i < response[0].length; i++) {
						atributes +=
							"<form class='row' action='' method='POST'>"+
							"<div class='form-group col'>"+
							"<label>"+response[0][i].atributo+
							"</label>"+
							"<input name='atributo_id_artibuto' type='hidden'>"+response[0][i].idAtributo+"</input>"+
							
							"</div>"+
							"<div class='form-group col'>"+
							"<input name='value'>"
							"</div>"+
							"<input name='producto_id_producto' type='hidden' value=''>"
							"</form>"

							"<label for='inputtext5' class='form-label'>"+response[0][i].atributo+"</label><input type='text' id='inputtext5' name='atributo_id_atributo' class='form-control' aria-describedby='textHelpBlock'>"

					}

					document.querySelector('[name="marca_id_marca"]').innerHTML = options
				})

				.catch((err) => {
					console.error("Error Servidor !!" + err.err)
				})
		})
}

function atributosProducto() {
	fetch("index.php?cl=atributo&me=visualizar")
		.then((response) => {
			if (response.ok == false || response.status > 299) {
				return Promise.reject({ err: "Error, no se encontro el archivo" })
			}
			return response.json()
		})
		.then((response) => {
			atributos = ""

			for (let i = 0; i < response.length; i++) {
				atributos += "<>"
			}

			document.querySelector('[name="marca_id_marca"]').innerHTML = atributos
		})

		.catch((err) => {
			console.error("Error Servidor !!" + err.err)
		})
}

function paginateEntrada(pages){

	cadena=""
	cadena+="<li class='page-item  small'><a class='page-link' onclick=entrada("+1+")>First</a></li>"
	for (let i = 1; i <=pages ; i++) {
		cadena+="<li class='page-item  small'><a class='page-link' onclick=entrada("+i+")>"+i+"</a></li>"		
	}
	cadena+="<li class='page-item  small'><a class='page-link' onclick=entrada("+pages+")>Last</a></li>"

	document.getElementById("pagination").innerHTML=cadena;
}

function entrada(id){
	fetch(URL+"entrada/lista/"+id, {
		method: "GET",
	})
		.then((response) => {
			if (response.ok == false || response.status > 299) {
				return Promise.reject({ err: "Error, no se encontro el archivo" })
				
			}
			return response.json()
		})
		.then((response) => {
			console.log(response)
			
			display(response[0])
			if (response[1]){
				document.getElementById('pagination').classList.remove('d-none');
				paginateEntrada(response[1]);
			}
			else{
				document.getElementById('pagination').classList.add('d-none');
			}
		})
		.catch((err) => {
			console.error(err)
		})
}


function paginateSalida(pages){

	cadena=""
	cadena+="<li class='page-item  small'><a class='page-link' onclick=salida("+1+")>First</a></li>"
	for (let i = 1; i <=pages ; i++) {
		cadena+="<li class='page-item  small'><a class='page-link' onclick=salida("+i+")>"+i+"</a></li>"		
	}
	cadena+="<li class='page-item  small'><a class='page-link' onclick=salida("+pages+")>Last</a></li>"

	document.getElementById("pagination").innerHTML=cadena;
}

function salida(id){
	fetch(URL+"salida/lista/"+id, {
		method: "GET",
	})
		.then((response) => {
			if (response.ok == false || response.status > 299) {
				return Promise.reject({ err: "Error, no se encontro el archivo" })
				
			}
			return response.json()
		})
		.then((response) => {
			console.log(response)
			
			display(response[0])
			if (response[1]){
				document.getElementById('pagination').classList.remove('d-none');
				paginateSalida(response[1]);
			}
			else{
				document.getElementById('pagination').classList.add('d-none');
			}
		})
		.catch((err) => {
			console.error(err)
		})
}



function productos(id) {
	fetch(URL+"product_test/list/"+id, {
		method: "GET",
	})
		.then((response) => {
			if (response.ok == false || response.status > 299) {
				return Promise.reject({ err: "Error, no se encontro el archivo" })
				
			}
			return response.json()
		})
		.then((response) => {
			console.log(response)
			
			display(response[0])
			if (response[1]){
				document.getElementById('pagination').classList.remove('d-none');
				paginateStock(response[1]);
			}
			else{
				document.getElementById('pagination').classList.add('d-none');
			}
		})
		.catch((err) => {
			console.error(err)
		})
}

function planeacion() {
	document
		.querySelector('[name="producto_id_producto"]')
		.addEventListener("focus", () => {
			fetch(URL+"product/list")
				.then((response) => {
					if (response.ok == false || response.status > 299) {
						return Promise.reject({ err: "Error, no se encontro el archivo" })
					}
					return response.json()
				})
				.then((response) => {
					options = ""

					for (let i = 0; i < response.length; i++) {
						options +=
							"<option value=" +
							response[i].Id +
							">" +
							response[i].Producto +
							"</option>"
					}

					document.querySelector('[name="producto_id_producto"]').innerHTML =
						options
				})

				.catch((err) => {
					console.error("Error Servidor !!" + err.err)
				})
		})
	document
		.querySelector('[name="proveedor_id_proveedor"]')
		.addEventListener("focus", () => {
			fetch(URL+"supplier/list")
				.then((response) => {
					if (response.ok == false || response.status > 299) {
						return Promise.reject({ err: "Error, no se encontro el archivo" })
					}
					return response.json()
				})
				.then((response) => {
					options = ""

					for (let i = 0; i < response[0].length; i++) {
						options +=
							"<option value=" +
							response[0][i].Id +
							">" +
							response[0][i].Nombre +
							"</option>"
					}

					document.querySelector('[name="proveedor_id_proveedor"]').innerHTML =
						options
				})

				.catch((err) => {
					console.error("Error Servidor !!" + err.err)
				})
		})
}
function gestionInventario(id) {
	fetch(URL+"purchaseOrder/list/"+id, {
		method: "GET",
	})

	.then((response) => {
			if (response.ok == false || response.status > 299) {
				return Promise.reject({ err: "Error, no se encontro el archivo" })
				response[0]
			}
			return response.json()
		})
		.then((response) => {
			console.log(response)

			

			$cabecera = document.querySelector("#tabla-admin > .cabecera")
			$cadenaCabecera = ""
			for (let i = 0; i < Object.keys(response[0][0]).length; i++) {
				$cadenaCabecera += "<th>" + Object.keys(response[0][0])[i] + "</th>"
			}
			$cadenaCabecera += "<th colspan=3>Accciones</th>"
			$cabecera.innerHTML = $cadenaCabecera
			$cuerpo = document.querySelector("#tabla-admin > .cuerpo")
			$cadenaCuerpo = ""
			for (let j = 0; j < response[0].length; j++) {
				$cadenaCuerpo += "<tr>"
				for (let k = 0; k < Object.keys(response[0][j]).length; k++) {
					$cadenaCuerpo +=
						"<td>" + response[0][j][Object.keys(response[0][0])[k]] + "</td>"
				}

				$cadenaCuerpo +=
					"<td><button type='button' class='btn btn-primary' data-bs-toggle='modal' data-bs-target='#Detalles'>Detalles</button><td><a href='"+URL+"entries/create/" +
					response[0][j][Object.keys(response[0][0])[0]] +
					"' class='btn btn-warning' >Gestionar Entradas</a></td><td><a href='"+URL+"outputs/create/" +
					response[0][j][Object.keys(response[0][0])[0]] +
					"' class='btn btn-warning'>Gestionar Salidas</a></td></tr>"
			}

			$cuerpo.innerHTML = $cadenaCuerpo

			if (response[1]){
				console.log(response[1])
				document.getElementById('pagination').classList.remove('d-none');
				paginateGestionInventario(response[1]);
			}
			else{
				document.getElementById('pagination').classList.add('d-none');
			}


			
		})
		.catch((err) => {
			console.error(err)
		})
}

function crearEntrada() {
	document
		.querySelector('[name="tipos_entrada_id_tipos_entrada"]')
		.addEventListener("focus", () => {
			fetch("index.php?cl=tipo_entrada&me=visualizar")
				.then((response) => {
					if (response.ok == false || response.status > 299) {
						return Promise.reject({ err: "Error, no se encontro el archivo" })
					}
					return response.json()
				})
				.then((response) => {
					options = ""

					for (let i = 0; i < response.length; i++) {
						options +=
							"<option value=" +
							response[i].Id +
							">" +
							response[i].Tipos +
							"</option>"
					}

					document.querySelector(
						'[name="tipos_entrada_id_tipos_entrada"]'
					).innerHTML = options
				})

				.catch((err) => {
					console.error("Error Servidor !!" + err.err)
				})
		})
}
function crearSalida() {
	document
		.querySelector('[name="id_tipo_salida"]')
		.addEventListener("focus", () => {
			fetch("index.php?cl=tipo_salida&me=visualizar")
				.then((response) => {
					if (response.ok == false || response.status > 299) {
						return Promise.reject({ err: "Error, no se encontro el archivo" })
					}
					return response.json()
				})
				.then((response) => {
					options = ""

					for (let i = 0; i < response.length; i++) {
						options +=
							"<option value=" +
							response[i].Id +
							">" +
							response[i].Tipos +
							"</option>"
					}

					document.querySelector('[name="id_tipo_salida"]').innerHTML = options
				})

				.catch((err) => {
					console.error("Error Servidor !!" + err.err)
				})
		})
}

function reportes(element) {
	fetch(URL+"reports/" + element, {
		method: "POST",
	})
		.then((response) => {
			if (response.ok == false || response.status > 299) {
				return Promise.reject({ err: "Error, no se encontro el archivo json" })
				response[0]
			}
			return response.json()
		})
		.then((response) => {
			console.log(response)
			$cabecera = document.querySelector("#tabla-admin > .cabecera")
			$cadenaCabecera = ""
			for (let i = 0; i < Object.keys(response[0]).length; i++) {
				$cadenaCabecera += "<th>" + Object.keys(response[0])[i] + "</th>"
			}
			$cabecera.innerHTML = $cadenaCabecera
			$cuerpo = document.querySelector("#tabla-admin > .cuerpo")
			$cadenaCuerpo = ""
			for (let j = 0; j < response.length; j++) {
				$cadenaCuerpo += "<tr>"
				for (let k = 0; k < Object.keys(response[j]).length; k++) {
					$cadenaCuerpo +=
						"<td>" + response[j][Object.keys(response[0])[k]] + "</td>"
				}
			}

			$cuerpo.innerHTML = $cadenaCuerpo
		})
		.catch((err) => {
			console.error(err)
		})
}

function updateAdmin(a) {
	$showed = document.querySelector("#popoverUpdate").classList.toggle("d-none")
	if (!$showed) {
		fetch(
			URL +
				document.getElementById("popup").getAttribute("data-pop") +
				"/update/" +
				a.dataset.id
		)
			.then((response) => {
				if (response.ok == false || response.status > 299) {
					return Promise.reject("Error, no se encontro el archivo")
				}
				return response.text()
			})
			.then((response) => {
				// console.log(response);
				document.querySelector("#popoverUpdate> div > div").innerHTML = response
			})

			.catch((err) => {
				console.error(err)
			})
	}
}
function deleteAdmin(a) {
	id = a.dataset.id

	fetch(
		URL +
			document.getElementById("popup").getAttribute("data-pop") +
			"/delete/" +
			id
	)
		.then((response) => {
			if (response.ok == false || response.status > 299) {
				return Promise.reject("Error, no se encontro el archivo")
			}
			return response.text()
		})
		.then((response) => {
			console.log(document.getElementById("popup").getAttribute("data-pop"),1)
			
			administracion(document.getElementById("popup").getAttribute("data-pop"),1)
		})
		.catch((err) => {
			console.error(err)
		})
}


function paginateGestionInventario(pages){

	cadena=""
	cadena+="<li class='page-item  small'><a class='page-link' onclick=gestionInventario("+1+")>First</a></li>"
	for (let i = 1; i <=pages ; i++) {
		cadena+="<li class='page-item  small'><a class='page-link' onclick=gestionInventario("+i+")>"+i+"</a></li>"		
	}
	cadena+="<li class='page-item  small'><a class='page-link' onclick=gestionInventario("+pages+")>Last</a></li>"

	document.getElementById("pagination").innerHTML=cadena;
}

function paginate(pages){
	$pop_= document.getElementById("popup").getAttribute("data-pop");
	console.log($pop_);

	cadena=""
	cadena+="<li class='page-item  small'><a class='page-link' onclick=administracion('"+$pop_+"',"+1+")>First</a></li>"
	for (let i = 1; i <=pages ; i++) {
		cadena+="<li class='page-item  small'><a class='page-link' onclick=administracion('"+$pop_+"',"+i+")>"+i+"</a></li>"		
	}
	cadena+="<li class='page-item  small'><a class='page-link' onclick=administracion('"+$pop_+"',"+pages+")>Last</a></li>"

	document.getElementById("pagination").innerHTML=cadena;
}

function paginateStock(pages){

	cadena=""
	cadena+="<li class='page-item  small'><a class='page-link' onclick=productos("+1+")>First</a></li>"
	for (let i = 1; i <=pages ; i++) {
		cadena+="<li class='page-item  small'><a class='page-link' onclick=productos("+i+")>"+i+"</a></li>"		
	}
	cadena+="<li class='page-item  small'><a class='page-link' onclick=productos("+pages+")>Last</a></li>"

	document.getElementById("pagination").innerHTML=cadena;
}
function test(id) {
	fetch(URL+"test/list")
		.then((response) => {
			if (response.ok == false || response.status > 299) {
				return Promise.reject({ err: "Error, no se encontro el archivo" })
				response[0]
			}
			return response.json()
		})
		.then((response) => {
			console.log(response)
			display(response[0]);
			// if (response[1]){
			// 	paginate(response[1]);
			// }
			
		})
		.catch((err) => {
			console.error(err)
		})
}


URL="http://localhost/IOIMR-prueba/";
d.addEventListener("DOMContentLoaded", () => {

	

	if (document.getElementById("administracion")) {
		$pop = document.getElementById("popup")

		document.querySelector("#tabla-admin > .cabecera").innerHTML =
			"Extrayendo Datos del servidor ..."
		document.querySelectorAll("[data-ruta]").forEach((e) => {
			e.addEventListener("click", (element) => {
				console.log($pop);
				$pop.setAttribute(
					"data-pop",
					element.currentTarget.getAttribute("data-ruta")
				)
				administracion(element.currentTarget.getAttribute("data-ruta"),1)
			})
		})
		setTimeout(() => {
			administracion(
				document.querySelector("[data-ruta]").getAttribute("data-ruta"),1
			)
			$pop.setAttribute("data-pop", "user")
		}, 2000)
		document.querySelectorAll(".pop").forEach((e) => {
			e.addEventListener("click", (el) => {
				$showed = document.querySelector("#popover").classList.toggle("d-none")
				if (!$showed) {
					console.log(
						URL +
							document.getElementById("popup").getAttribute("data-pop") +
							"/showCreate"
					)
					fetch(
						URL +
							document.getElementById("popup").getAttribute("data-pop") +
							"/showCreate"
					)
						.then((response) => {
							if (response.ok == false || response.status > 299) {
								return Promise.reject("Error, no se encontro el archivo")
							}
							return response.text()
						})
						.then((response) => {
							document.querySelector("#popover> div > div").innerHTML = response
						})
						.catch((err) => {
							console.error(err)
						})
				}
			})
		})
	} else if (document.getElementById("productos")) {
		document.querySelector("#tabla-admin > .cabecera").innerHTML =
			"Extrayendo Datos del servidor ..."
		productos(1)
	} else if (document.getElementById("crearProducto") ) {
		crearProducto()
	
	}
	else if (document.getElementById("entradas") ) {
		entrada(1)
	}
	else if (document.getElementById("salidas") ) {
		salida(1)
	}
	else if (document.getElementById("atributosProducto")) {
		atributosProducto()
	} else if (document.getElementById("planeacion")) {
		planeacion()
	} else if (document.getElementById("gestionInventario")) {
		gestionInventario(1)
	} else if (document.getElementById("crearEntrada")) {
		crearEntrada()
	} else if (document.getElementById("crearSalida")) {
		crearSalida()
	} else if (document.getElementById("test")) {
			test();
		
		


	} else if (document.getElementById("reportes")) {
		document.querySelectorAll("[data-reporte]").forEach((e) => {
			e.addEventListener("click", (element) => {
				reportes(element.currentTarget.getAttribute("data-reporte"))
			})
		})
	}
})

// --------------------------------------------------------------------------------------------------------------
