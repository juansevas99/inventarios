<?php


$ruta=explode("/",$_SERVER['REQUEST_URI']);
$clase=!empty($ruta[2])?$ruta[2]:"";
$metodo=!empty($ruta[3])?$ruta[3]:"";
$_GET["id"]=!empty($ruta[4])?$ruta[4]:"";
// $clase=!empty($ruta[1])?$ruta[1]:"";
// $metodo=!empty($ruta[2])?$ruta[2]:"";
// $_GET["id"]=!empty($ruta[3])?$ruta[3]:"";

unset($_GET["var"]);
$ruta=$clase."/".$metodo;
    

//test

web::registrarRutas("test/","test","run");



// Cree ls rurutas/admtas aqui debajo
web::registrarRutas("/","rutas","home");
web::registrarRutas("routes/management","rutas","apps");
web::registrarRutas("routes/opcionOfManagement","rutas","opcionOfManagement");
web::registrarRutas("routes/","rutas","ajustes");
web::registrarRutas("routes/management","rutas","ordenCompra");
web::registrarRutas("routes/products","rutas","productos");
web::registrarRutas("routes/planOrder","rutas","planeacion");
web::registrarRutas("routes/reports","rutas","reportes");
web::registrarRutas("routes/error","rutas","error");
web::registrarRutas("routes/errorSQL","rutas","errorSQL");
web::registrarRutas("routes/errorSQL","rutas","errorSQL");
web::registrarRutas("routes/test","rutas","test");
web::registrarRutas("routes/login","rutas","login");
web::registrarRutas("routes/login","rutas","login");




//product

web::registrarRutas("product/list","rutas","productos");
web::registrarRutas("product/listActives","producto","visualizarEstadoActivo");
web::registrarRutas("product/create","producto","insertar");
web::registrarRutas("product/createForm","producto","insertarForm");
web::registrarRutas("product/update","producto","actualizar");
web::registrarRutas("product/delete","producto","delete");
web::registrarRutas("product/showCreate","producto","prepararCreacion");
web::registrarRutas("product/confirmUpdate","producto","confirmarActualizar");
#proveedor

web::registrarRutas("supplier/list","proveedor","visualizar");
web::registrarRutas("supplier/listInout","proveedor","visualizarLista");
web::registrarRutas("supplier/create","proveedor","insertar");
web::registrarRutas("supplier/update","proveedor","actualizar");
web::registrarRutas("supplier/delete","proveedor","delete");
web::registrarRutas("supplier/showCreate","proveedor","prepararCreacion");
web::registrarRutas("supplier/confirmUpdate","proveedor","confirmarActualizar");

#brand
web::registrarRutas("brand/list","marca","visualizar");
web::registrarRutas("brand/create","marca","insertar");
web::registrarRutas("brand/update","marca","actualizar");
web::registrarRutas("brand/delete","marca","delete");
web::registrarRutas("brand/showCreate","marca","prepararCreacion");
web::registrarRutas("brand/confirmUpdate","marca","confirmarActualizar");



#features
web::registrarRutas("atributo/list","atributo","visualizar");
web::registrarRutas("atributo/","atributo","displyatributolist");
web::registrarRutas("features/create","atributo","insertar");
web::registrarRutas("features/update","atributo","actualizar");
web::registrarRutas("features/delete","atributo","delete");
web::registrarRutas("features/showCreate","atributo","prepararCreacion");
web::registrarRutas("features/confirmUpdate","atributo","confirmarActualizar");


#measures
web::registrarRutas("measure/list","medida","visualizar");
web::registrarRutas("measure/create","medida","insertar");
web::registrarRutas("measure/update","medida","actualizar");
web::registrarRutas("measure/delete","medida","delete");
web::registrarRutas("measure/showCreate","medida","prepararCreacion");
web::registrarRutas("measure/confirmUpdate","medida","confirmarActualizar");

#type_outputs
web::registrarRutas("type_outputs/list","tipo_salida","visualizar");
web::registrarRutas("type_outputs/create","tipo_salida","insertar");
web::registrarRutas("type_outputs/update","tipo_salida","actualizar");
web::registrarRutas("type_outputs/delete","tipo_salida","delete");
web::registrarRutas("type_outputs/showCreate","tipo_salida","prepararCreacion");
web::registrarRutas("type_outputs/confirmUpdate","tipo_salida","confirmarActualizar");


#type_entries
web::registrarRutas("type_entries/list","tipo_entrada","visualizar");
web::registrarRutas("type_entries/create","tipo_entrada","insertar");
web::registrarRutas("type_entries/update","tipo_entrada","actualizar");
web::registrarRutas("type_entries/delete","tipo_entrada","delete");
web::registrarRutas("type_entries/showCreate","tipo_entrada","prepararCreacion");
web::registrarRutas("type_entries/confirmUpdate","tipo_entrada","confirmarActualizar");


#purchaseOrder
web::registrarRutas("purchaseOrder/list","ordenCompra","visualizar");
web::registrarRutas("purchaseOrder/create","ordenCompra","insertar");
web::registrarRutas("purchaseOrder/update","ordenCompra","actualizar");
web::registrarRutas("purchaseOrder/delete","ordenCompra","delete");
web::registrarRutas("purchaseOrder/showCreate","ordenCompra","prepararCreacion");
web::registrarRutas("purchaseOrder/confirmUpdate","ordenCompra","confirmarActualizar");


#category
web::registrarRutas("categoriaaa","categoria","visualizar");
web::registrarRutas("category/create","categoria","insertar");
web::registrarRutas("category/update","categoria","actualizar");
web::registrarRutas("category/delete","categoria","delete");
web::registrarRutas("category/showCreate","categoria","prepararCreacion");
web::registrarRutas("category/confirmUpdate","categoria","confirmarActualizar");


#rol
// web::registrarRutas("rol/list","rol","visualizar");
// web::registrarRutas("rol/create","rol","insertar");
// web::registrarRutas("rol/update","rol","actualizar");
// web::registrarRutas("rol/delete","rol","delete");
// web::registrarRutas("rol/showCreate","rol","prepararCreacion");
// web::registrarRutas("rol/confirmUpdate","rol","confirmarActualizar");




#reports

web::registrarRutas("reports/kardex","reportes","kardex");
web::registrarRutas("reports/purchaseOrders","reportes","ordenes");
web::registrarRutas("reports/bestsellers","reportes","masVendidos");
web::registrarRutas("reports/entries","reportes","entradas");
web::registrarRutas("reports/outputs","reportes","salidas");



#estado

web::registrarRutas("state/list","estado","visualizar");
web::registrarRutas("state/create","estado","insertar");
web::registrarRutas("state/update","estado","actualizar");
web::registrarRutas("state/delete","estado","delete");
web::registrarRutas("state/showCreate","estado","prepararCreacion");
web::registrarRutas("state/confirmUpdate","estado","confirmarActualizar");





//entries
web::registrarRutas("input/list","pedido_entrada","visualizar");
web::registrarRutas("input/","pedido_entrada","displayInputs");
web::registrarRutas("input/displayFormMovEntrada","pedido_entrada","displayFormMovEntrada");
web::registrarRutas("input/createOnepart","pedido_entrada","create");
web::registrarRutas("input/visualizarCrear","pedido_entrada","visualizarProveedor_pedido");
web::registrarRutas("input/formCreate","pedido_entrada","displayFormCreate");
web::registrarRutas("input/update","pedido_entrada","actualizar");
web::registrarRutas("input/delete","pedido_entrada","delete");
web::registrarRutas("input/showCreate","pedido_entrada","prepararCreacion");
web::registrarRutas("input/confirmUpdate","pedido_entrada","confirmarActualizar");
web::registrarRutas("input/actualizar","pedido_entrada","actualizar");




//outputs
web::registrarRutas("outputs/list","pedido_salida","visualizar");
web::registrarRutas("outputs/","pedido_salida","displayOutputs");
web::registrarRutas("outputs/create","pedido_salida","insertar");
web::registrarRutas("outputs/update","pedido_salida","actualizar");
web::registrarRutas("outputs/delete","pedido_salida","delete");
web::registrarRutas("outputs/showCreate","pedido_salida","prepararCreacion");
web::registrarRutas("outputs/confirmUpdate","pedido_salida","confirmarActualizar");



#test

web::registrarRutas("test/list","test","probe");

#prueba




web::registrarRutas("factuacion/list","rol","visualizar");
web::registrarRutas("factuacion/create","rol","insertar");
web::registrarRutas("factuacion/update","rol","actualizar");
web::registrarRutas("factuacion/delete","rol","delete");




web::registrarRutas("routes/teste","rutas","david");
web::registrarRutas("routes/richard","rutas","richard");

web::registrarRutas("routes/reports","rutas","reportes");

web::registrarRutas("test/delete","test","borrar");
web::registrarRutas("test/create","test","crear");


web::registrarRutas("atributoscat/list","categoria_has_atributo","visualizar");




///user

web::registrarRutas("user/login","user","login");
web::registrarRutas("user/close","user","cerrarSesion");
web::registrarRutas("user/","user","displayusuarioview");
web::registrarRutas("user/list","user","visualizar");
web::registrarRutas("crearuser/","user","displaycreateuser");
web::registrarRutas("user/create","user","create");


//producto
web::registrarRutas("product_test/list","producto_test","visualizarEstadoActivo");
web::registrarRutas("producto/","producto_test","displayproductoview");
web::registrarRutas("crearProducto/","producto_test","displaycreateProduct");
web::registrarRutas("producto/create","producto_test","create");

web::registrarRutas("producto/listForMov","producto_test","getProductos");





//Pedido_entrada
web::registrarRutas("inventario/","rutas","inventario");

//categoria
web::registrarRutas("categoria/","categoria","displaycategoriaview");

web::registrarRutas("categoria/listar","categoria","visualizar");

web::registrarRutas("category/list","categoria","categoriaList");


//pedido

web::registrarRutas("pedidos/list","pedidos","visualizar");


//roles



web::registrarRutas("roles/list","roles","displayList");
web::registrarRutas("roles/","roles","displayrolesview");
web::registrarRutas("roles/displyForm","roles","displaycreateroles");
web::registrarRutas("roles/list","roles","visualizar");
web::registrarRutas("roles/create","roles","create");

//proveedor

web::registrarRutas("proveedor/","proveedor","displayproveedorview");
web::registrarRutas("proveedor/visualizarProveedor","proveedor","visualizarProveedor");

web::registrarRutas("proveedor/list","proveedor","visualizar");
web::registrarRutas("proveedor/displayCrear","proveedor","displaycreateProveedor");

web::registrarRutas("proveedor/create","proveedor","create");


//movimiento_entrada


web::registrarRutas("movimiento_entrada/list","movimiento_entrada","getOne");
web::registrarRutas("movimiento_entrada/create","movimiento_entrada","create");



