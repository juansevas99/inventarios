-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: bgixzn7c0yfjxrclveam-mysql.services.clever-cloud.com:3306
-- Generation Time: Jan 21, 2022 at 11:06 PM
-- Server version: 8.0.22-13
-- PHP Version: 7.2.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bgixzn7c0yfjxrclveam`
--

-- --------------------------------------------------------

--
-- Table structure for table `atributo`
--

CREATE TABLE `atributo` (
  `id_atributo` int NOT NULL,
  `atributo` varchar(45) NOT NULL,
  `medida_id_medida` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `atributo`
--

INSERT INTO `atributo` (`id_atributo`, `atributo`, `medida_id_medida`) VALUES
(1, 'Memoria RAM', 1),
(2, 'Memoria Disco Duro', 2),
(3, 'Resolucion camara', 5),
(4, 'Tamaño pantalla', 6);

-- --------------------------------------------------------

--
-- Table structure for table `atributo_has_categoria`
--

CREATE TABLE `atributo_has_categoria` (
  `atributo_id_atributo` int NOT NULL,
  `categoria_id_categoria` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `atributo_has_producto`
--

CREATE TABLE `atributo_has_producto` (
  `atributo_id_atributo` int NOT NULL,
  `producto_id_producto` int NOT NULL,
  `valor` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `categoria`
--

CREATE TABLE `categoria` (
  `id_categoria` int NOT NULL,
  `categoria` varchar(45) NOT NULL,
  `descripcion` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `categoria`
--

INSERT INTO `categoria` (`id_categoria`, `categoria`, `descripcion`) VALUES
(1, 'desktop PC', NULL),
(2, 'laptop', NULL),
(3, 'camara', NULL),
(4, 'Parlantes', NULL),
(5, 'Watch', NULL),
(6, 'Audifonos', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `entrada`
--

CREATE TABLE `entrada` (
  `observaciones` varchar(250) DEFAULT '',
  `movimiento_id_movimiento` int NOT NULL,
  `pedido_entrada_pedido_id_pedido` int NOT NULL,
  `stock` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `entrada`
--

INSERT INTO `entrada` (`observaciones`, `movimiento_id_movimiento`, `pedido_entrada_pedido_id_pedido`, `stock`) VALUES
(NULL, 1, 1, 34),
(NULL, 2, 1, 34),
(NULL, 3, 1, 3),
(NULL, 4, 2, 3),
(NULL, 5, 2, 7),
(NULL, 6, 2, 5),
(' ', 14, 31, 45),
(' ', 15, 32, 3);

-- --------------------------------------------------------

--
-- Table structure for table `estado`
--

CREATE TABLE `estado` (
  `id_estado` int NOT NULL,
  `estado` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `estado`
--

INSERT INTO `estado` (`id_estado`, `estado`) VALUES
(1, 'activo'),
(2, 'inactivo');

-- --------------------------------------------------------

--
-- Table structure for table `estado_pedido`
--

CREATE TABLE `estado_pedido` (
  `id_estado` int NOT NULL,
  `estado` varchar(45) NOT NULL,
  `descripcion` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `estado_pedido`
--

INSERT INTO `estado_pedido` (`id_estado`, `estado`, `descripcion`) VALUES
(1, 'pendiente', 'Apenas se creo el pedido');

-- --------------------------------------------------------

--
-- Table structure for table `medida`
--

CREATE TABLE `medida` (
  `id_medida` int NOT NULL,
  `medida` varchar(45) NOT NULL,
  `descripcion` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `medida`
--

INSERT INTO `medida` (`id_medida`, `medida`, `descripcion`) VALUES
(1, 'GB', 'Gigabyte'),
(2, 'TB', 'TeraByte'),
(3, 'Pulgadas', 'Pulgadas'),
(4, 'mts', 'metros'),
(5, 'Px', 'pixeles'),
(6, 'dB', 'desibeles');

-- --------------------------------------------------------

--
-- Table structure for table `movimiento`
--

CREATE TABLE `movimiento` (
  `id_movimiento` int NOT NULL,
  `cantidad` varchar(45) DEFAULT NULL,
  `precio_unitario` decimal(10,2) DEFAULT NULL,
  `estado_id_estado` int NOT NULL,
  `producto_id_producto` int NOT NULL,
  `hecho` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `movimiento`
--

INSERT INTO `movimiento` (`id_movimiento`, `cantidad`, `precio_unitario`, `estado_id_estado`, `producto_id_producto`, `hecho`) VALUES
(1, '12', '1200000.00', 1, 1, 2),
(2, '23', '324234.00', 1, 1, 2),
(3, '34', '235644.00', 2, 2, 44),
(4, '23', '30000.00', 1, 1, 2),
(5, '23', '40000.00', 1, 3, 55),
(6, '34', '700000.00', 1, 4, 3),
(7, '32', '45678.00', 1, 4, 5),
(13, '45', '1651451.00', 1, 12, 45),
(14, '45', '1651451.00', 1, 12, 45),
(15, '3', '34567890.00', 1, 11, 3);

-- --------------------------------------------------------

--
-- Table structure for table `pedido`
--

CREATE TABLE `pedido` (
  `id_pedido` int NOT NULL,
  `estado_pedido_estado` int DEFAULT NULL,
  `fecha_creacion` datetime DEFAULT CURRENT_TIMESTAMP,
  `fecha_efectiva` datetime DEFAULT CURRENT_TIMESTAMP,
  `entrada_salida` varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pedido`
--

INSERT INTO `pedido` (`id_pedido`, `estado_pedido_estado`, `entrada_salida`) VALUES
(1, NULL, 'E'),
(2, NULL, 'E'),
(3, NULL, 'E'),
(4, NULL, 'E'),
(5, NULL, 'E'),
(6, NULL, 'E'),
(7, NULL, 'S'),
(9, 1, 'E'),
(10, 1, 'E'),
(11, 1, 'E'),
(12, 1, 'E'),
(13, 1, 'E'),
(14, 1, 'E'),
(15, 1, 'E'),
(16, 1, 'E'),
(17, 1, 'E'),
(18, 1, 'E'),
(19, 1, 'E'),
(20, 1, 'E'),
(21, 1, 'E'),
(22, 1, 'E'),
(23, 1, 'E'),
(24, 1, 'E'),
(25, 1, 'E'),
(26, 1, 'E'),
(27, 1, 'E'),
(28, 1, 'E'),
(29, 1, 'E'),
(30, 1, 'E'),
(31, 1, 'E'),
(32, 1, 'E');

-- --------------------------------------------------------

--
-- Table structure for table `pedido_entrada`
--

CREATE TABLE `pedido_entrada` (
  `observaciones` varchar(210) DEFAULT '',
  `pedido_id_pedido` int NOT NULL,
  `proveedor_id_proveedor` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pedido_entrada`
--

INSERT INTO `pedido_entrada` (`observaciones`, `pedido_id_pedido`, `proveedor_id_proveedor`) VALUES
(NULL, 1, 1),
(NULL, 2, 1),
(NULL, 3, 2),
(NULL, 4, 3),
(NULL, 5, 4),
(NULL, 6, 3),
('Esta es un a´prueba', 15, 3),
('Segunda prueba', 16, 4),
(' \r\n                ', 17, 3),
(' \r\n                ', 18, 3),
(' \r\n                ', 19, 8),
(' \r\n                ', 20, 7),
(' \r\n                ', 21, 5),
(' \r\n                ', 22, 7),
(' \r\n                ', 23, 4),
(' \r\n                ', 24, 6),
(' \r\n                ', 25, 5),
(' \r\n                ', 26, 6),
(' \r\n                ', 27, 6),
(' \r\n                ', 28, 8),
(' \r\n                ', 29, 6),
(' \r\n                ', 30, 6),
(' \r\n                ', 31, 3),
(' \r\n                ', 32, 7);

-- --------------------------------------------------------

--
-- Table structure for table `pedido_salida`
--

CREATE TABLE `pedido_salida` (
  `observaciones` int DEFAULT NULL,
  `pedido_id_pedido` int NOT NULL,
  `destino` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pedido_salida`
--

INSERT INTO `pedido_salida` (`observaciones`, `pedido_id_pedido`, `destino`) VALUES
(NULL, 7, 'Customer');

-- --------------------------------------------------------

--
-- Table structure for table `privilegies`
--

CREATE TABLE `privilegies` (
  `roles_Id_rol` int NOT NULL,
  `routes_Id_routes` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `producto`
--

CREATE TABLE `producto` (
  `id_producto` int NOT NULL,
  `categoria_id_categoria` int DEFAULT NULL,
  `precioUnitario` decimal(10,1) DEFAULT '0.0',
  `stock_inicial` int DEFAULT '0',
  `fecha_creacion` datetime DEFAULT CURRENT_TIMESTAMP,
  `stock_actual` varchar(45) NOT NULL DEFAULT '0',
  `producto` varchar(45) DEFAULT 'N/A',
  `estado` int DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `producto`
--

INSERT INTO `producto` (`id_producto`, `categoria_id_categoria`, `precioUnitario`, `stock_inicial`, `stock_actual`, `producto`, `estado`) VALUES
(1, 1, '234444.0', 0, '0', 'NA', 1),
(2, 1, '342333.0', 23, '3', 'NA', 1),
(3, 2, '30000.0', 2, '2', 'NA', 1),
(4, 2, '97654.0', 2, '2', 'NA', 1),
(5, 3, '90000.0', 4, '3', 'NA', 1),
(6, 4, '344444.0', 2, '2', 'NA', 1),
(7, 5, '98777.0', 3, '2', 'NA', 1),
(8, NULL, '0.0', 0, '0', 'NA', 1),
(9, NULL, '0.0', 0, '0', 'NA', 1),
(10, NULL, '0.0', 0, '0', 'NA', 1),
(11, 1, '10000.0', 0, '0', 'Prueba', 1),
(12, 1, '10000.0', 0, '0', 'Prueba', 1),
(13, 2, '1500000.0', 13, '20', 'Computador M12 Lenovo', 1),
(14, 2, '1500000.0', 13, '20', 'Computador M12 Lenovo', 1),
(15, 2, '1500000.0', 13, '20', 'Computador M12 Lenovo', 1),
(16, 2, '1500000.0', 13, '20', 'Computador M12 Lenovo', 1),
(17, 1, '3000000.0', 12, '12', 'Computador ASUS SDL', 0),
(18, NULL, '0.0', 0, '0', NULL, 1),
(19, NULL, '0.0', 0, '0', NULL, 1),
(20, NULL, '0.0', 0, '0', NULL, 1),
(21, 6, '4500000.0', 0, '0', 'Camara HD Ms', 1),
(22, 6, '2000.0', 20, '20', 'papas', 1),
(23, 6, '6100000.0', 15, '15', 'All in opne Desktop Computer', 0),
(24, 3, '30000.0', 2, '0', 'Camara', 1),
(25, 2, '3600000.0', 10, '2', 'Portatil', 1);

-- --------------------------------------------------------

--
-- Table structure for table `producto_has_proveedor`
--

CREATE TABLE `producto_has_proveedor` (
  `producto_id_producto` int NOT NULL,
  `proveedor_id_proveedor` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `proveedor`
--

CREATE TABLE `proveedor` (
  `id_proveedor` int NOT NULL,
  `contacto` varchar(45) DEFAULT NULL,
  `telefono` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `proveedor`
--

INSERT INTO `proveedor` (`id_proveedor`, `contacto`, `telefono`) VALUES
(1, 'MR', '3232397'),
(2, 'SS', '123123'),
(3, 'MER_Y', '12312545'),
(4, 'TEC_H', '6757545'),
(5, 'LES', '39875464'),
(6, 'LOER', '3467823'),
(7, 'DDD', '8765'),
(8, 'Samsung', '2346324'),
(9, NULL, NULL),
(10, NULL, NULL),
(11, NULL, NULL),
(12, NULL, NULL),
(13, NULL, NULL),
(14, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `Id_rol` int NOT NULL,
  `rol_name` varchar(45) NOT NULL,
  `description` varchar(250) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`Id_rol`, `rol_name`, `description`) VALUES
(1, 'Admin', 'administrador de la pagina'),
(2, 'Surtidor', 'Persona que surte lo que hay en bodega'),
(3, 'Validador', 'caldasd'),
(4, 'Visitante', 'Se encarga de la verificacion propia de la funcionalidad del sistema'),
(5, 'Sponsor', '');

-- --------------------------------------------------------

--
-- Table structure for table `routes`
--

CREATE TABLE `routes` (
  `id_routes` int NOT NULL,
  `routes_name` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `salida`
--

CREATE TABLE `salida` (
  `observaciones` int DEFAULT NULL,
  `movimiento_id_movimiento` int NOT NULL,
  `pedido_salida_pedido_id_pedido` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `salida`
--

INSERT INTO `salida` (`observaciones`, `movimiento_id_movimiento`, `pedido_salida_pedido_id_pedido`) VALUES
(NULL, 7, 7);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id_user` int NOT NULL,
  `email` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `contact_number` varchar(45) DEFAULT NULL,
  `user_name` varchar(45) NOT NULL,
  `roles_Id_rol` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `email`, `password`, `contact_number`, `user_name`, `roles_Id_rol`) VALUES
(1, 'juansevas992010@gmail.com', 'juansebas9913', '3023855434', 'jgalvis', 1),
(2, 'John.paladinesua26@gmail.com', 'john26', '3042504888', 'jhon26', 1),
(3, 'Robregon67@@misena.edu.co', 'Richard67', '3219080889', 'Richard67', 1),
(4, 'narodriguez372@misena.edu.co', 'nestor372', '', 'Nestor', 1),
(5, 'damoya837@gmai.com', '123456', '304254840', 'David Moya ibarra', 2),
(6, 'test@gmail.com', 'juansebas9913', '3023864918', 'test', 2),
(7, 'danielaprueba@gmail.com', '123456789', '3021574515', 'Daniela', 3),
(8, 'daniela@gmail.com', '123456789', '30221486465', 'Daniela', 4),
(9, 'jhrondon9@misena.edu.co', '12345', '3332224455', 'JOHN HENRY RONDON SUAREZ', 4);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `atributo`
--
ALTER TABLE `atributo`
  ADD PRIMARY KEY (`id_atributo`),
  ADD UNIQUE KEY `atributo_UNIQUE` (`atributo`),
  ADD KEY `fk_atributo_medida1_idx` (`medida_id_medida`);

--
-- Indexes for table `atributo_has_categoria`
--
ALTER TABLE `atributo_has_categoria`
  ADD PRIMARY KEY (`atributo_id_atributo`,`categoria_id_categoria`),
  ADD KEY `fk_atributo_has_categoria_categoria1_idx` (`categoria_id_categoria`),
  ADD KEY `fk_atributo_has_categoria_atributo1_idx` (`atributo_id_atributo`);

--
-- Indexes for table `atributo_has_producto`
--
ALTER TABLE `atributo_has_producto`
  ADD PRIMARY KEY (`atributo_id_atributo`,`producto_id_producto`),
  ADD KEY `fk_atributo_has_producto_producto1_idx` (`producto_id_producto`),
  ADD KEY `fk_atributo_has_producto_atributo1_idx` (`atributo_id_atributo`);

--
-- Indexes for table `categoria`
--
ALTER TABLE `categoria`
  ADD PRIMARY KEY (`id_categoria`),
  ADD UNIQUE KEY `categoria_UNIQUE` (`categoria`);

--
-- Indexes for table `entrada`
--
ALTER TABLE `entrada`
  ADD PRIMARY KEY (`movimiento_id_movimiento`),
  ADD KEY `fk_entrada_pedido_entrada1_idx` (`pedido_entrada_pedido_id_pedido`);

--
-- Indexes for table `estado`
--
ALTER TABLE `estado`
  ADD PRIMARY KEY (`id_estado`),
  ADD UNIQUE KEY `estado_UNIQUE` (`estado`);

--
-- Indexes for table `estado_pedido`
--
ALTER TABLE `estado_pedido`
  ADD PRIMARY KEY (`id_estado`),
  ADD UNIQUE KEY `estado_UNIQUE` (`estado`);

--
-- Indexes for table `medida`
--
ALTER TABLE `medida`
  ADD PRIMARY KEY (`id_medida`),
  ADD UNIQUE KEY `medida_UNIQUE` (`medida`);

--
-- Indexes for table `movimiento`
--
ALTER TABLE `movimiento`
  ADD PRIMARY KEY (`id_movimiento`),
  ADD KEY `fk_movimiento_estado1_idx` (`estado_id_estado`),
  ADD KEY `fk_movimiento_producto1_idx` (`producto_id_producto`);

--
-- Indexes for table `pedido`
--
ALTER TABLE `pedido`
  ADD PRIMARY KEY (`id_pedido`),
  ADD KEY `fk_pedido_estado_pedido1_idx` (`estado_pedido_estado`);

--
-- Indexes for table `pedido_entrada`
--
ALTER TABLE `pedido_entrada`
  ADD PRIMARY KEY (`pedido_id_pedido`),
  ADD KEY `fk_pedido_entrada_proveedor1_idx` (`proveedor_id_proveedor`);

--
-- Indexes for table `pedido_salida`
--
ALTER TABLE `pedido_salida`
  ADD PRIMARY KEY (`pedido_id_pedido`);

--
-- Indexes for table `privilegies`
--
ALTER TABLE `privilegies`
  ADD PRIMARY KEY (`roles_Id_rol`,`routes_Id_routes`),
  ADD KEY `fk_roles_has_routes_routes1_idx` (`routes_Id_routes`),
  ADD KEY `fk_roles_has_routes_roles1_idx` (`roles_Id_rol`);

--
-- Indexes for table `producto`
--
ALTER TABLE `producto`
  ADD PRIMARY KEY (`id_producto`),
  ADD KEY `fk_producto_categoria1_idx` (`categoria_id_categoria`);

--
-- Indexes for table `producto_has_proveedor`
--
ALTER TABLE `producto_has_proveedor`
  ADD PRIMARY KEY (`producto_id_producto`,`proveedor_id_proveedor`),
  ADD KEY `fk_producto_has_proveedor_proveedor1_idx` (`proveedor_id_proveedor`),
  ADD KEY `fk_producto_has_proveedor_producto1_idx` (`producto_id_producto`);

--
-- Indexes for table `proveedor`
--
ALTER TABLE `proveedor`
  ADD PRIMARY KEY (`id_proveedor`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`Id_rol`);

--
-- Indexes for table `routes`
--
ALTER TABLE `routes`
  ADD PRIMARY KEY (`id_routes`);

--
-- Indexes for table `salida`
--
ALTER TABLE `salida`
  ADD PRIMARY KEY (`movimiento_id_movimiento`),
  ADD KEY `fk_salida_movimiento1_idx` (`movimiento_id_movimiento`),
  ADD KEY `fk_salida_pedido_salida1_idx` (`pedido_salida_pedido_id_pedido`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`),
  ADD UNIQUE KEY `email_UNIQUE` (`email`),
  ADD KEY `fk_User_roles_idx` (`roles_Id_rol`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `atributo`
--
ALTER TABLE `atributo`
  MODIFY `id_atributo` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `categoria`
--
ALTER TABLE `categoria`
  MODIFY `id_categoria` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `estado`
--
ALTER TABLE `estado`
  MODIFY `id_estado` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `estado_pedido`
--
ALTER TABLE `estado_pedido`
  MODIFY `id_estado` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `medida`
--
ALTER TABLE `medida`
  MODIFY `id_medida` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `movimiento`
--
ALTER TABLE `movimiento`
  MODIFY `id_movimiento` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `pedido`
--
ALTER TABLE `pedido`
  MODIFY `id_pedido` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `producto`
--
ALTER TABLE `producto`
  MODIFY `id_producto` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `proveedor`
--
ALTER TABLE `proveedor`
  MODIFY `id_proveedor` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `Id_rol` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `routes`
--
ALTER TABLE `routes`
  MODIFY `id_routes` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `atributo`
--
ALTER TABLE `atributo`
  ADD CONSTRAINT `fk_atributo_medida1` FOREIGN KEY (`medida_id_medida`) REFERENCES `medida` (`id_medida`);

--
-- Constraints for table `atributo_has_categoria`
--
ALTER TABLE `atributo_has_categoria`
  ADD CONSTRAINT `fk_atributo_has_categoria_atributo1` FOREIGN KEY (`atributo_id_atributo`) REFERENCES `atributo` (`id_atributo`),
  ADD CONSTRAINT `fk_atributo_has_categoria_categoria1` FOREIGN KEY (`categoria_id_categoria`) REFERENCES `categoria` (`id_categoria`);

--
-- Constraints for table `atributo_has_producto`
--
ALTER TABLE `atributo_has_producto`
  ADD CONSTRAINT `fk_atributo_has_producto_atributo1` FOREIGN KEY (`atributo_id_atributo`) REFERENCES `atributo` (`id_atributo`),
  ADD CONSTRAINT `fk_atributo_has_producto_producto1` FOREIGN KEY (`producto_id_producto`) REFERENCES `producto` (`id_producto`);

--
-- Constraints for table `entrada`
--
ALTER TABLE `entrada`
  ADD CONSTRAINT `fk_entrada_movimiento1` FOREIGN KEY (`movimiento_id_movimiento`) REFERENCES `movimiento` (`id_movimiento`),
  ADD CONSTRAINT `fk_entrada_pedido_entrada1` FOREIGN KEY (`pedido_entrada_pedido_id_pedido`) REFERENCES `pedido_entrada` (`pedido_id_pedido`);

--
-- Constraints for table `movimiento`
--
ALTER TABLE `movimiento`
  ADD CONSTRAINT `fk_movimiento_estado1` FOREIGN KEY (`estado_id_estado`) REFERENCES `estado` (`id_estado`),
  ADD CONSTRAINT `fk_movimiento_producto1` FOREIGN KEY (`producto_id_producto`) REFERENCES `producto` (`id_producto`);

--
-- Constraints for table `pedido`
--
ALTER TABLE `pedido`
  ADD CONSTRAINT `fk_pedido_estado_pedido1` FOREIGN KEY (`estado_pedido_estado`) REFERENCES `estado_pedido` (`id_estado`);

--
-- Constraints for table `pedido_entrada`
--
ALTER TABLE `pedido_entrada`
  ADD CONSTRAINT `fk_pedido_entrada_pedido` FOREIGN KEY (`pedido_id_pedido`) REFERENCES `pedido` (`id_pedido`),
  ADD CONSTRAINT `fk_pedido_entrada_proveedor1` FOREIGN KEY (`proveedor_id_proveedor`) REFERENCES `proveedor` (`id_proveedor`);

--
-- Constraints for table `pedido_salida`
--
ALTER TABLE `pedido_salida`
  ADD CONSTRAINT `fk_pedido_salida_pedido1` FOREIGN KEY (`pedido_id_pedido`) REFERENCES `pedido` (`id_pedido`);

--
-- Constraints for table `privilegies`
--
ALTER TABLE `privilegies`
  ADD CONSTRAINT `fk_roles_has_routes_roles1` FOREIGN KEY (`roles_Id_rol`) REFERENCES `roles` (`Id_rol`),
  ADD CONSTRAINT `fk_roles_has_routes_routes1` FOREIGN KEY (`routes_Id_routes`) REFERENCES `routes` (`id_routes`);

--
-- Constraints for table `producto`
--
ALTER TABLE `producto`
  ADD CONSTRAINT `fk_producto_categoria1` FOREIGN KEY (`categoria_id_categoria`) REFERENCES `categoria` (`id_categoria`);

--
-- Constraints for table `producto_has_proveedor`
--
ALTER TABLE `producto_has_proveedor`
  ADD CONSTRAINT `fk_producto_has_proveedor_producto1` FOREIGN KEY (`producto_id_producto`) REFERENCES `producto` (`id_producto`),
  ADD CONSTRAINT `fk_producto_has_proveedor_proveedor1` FOREIGN KEY (`proveedor_id_proveedor`) REFERENCES `proveedor` (`id_proveedor`);

--
-- Constraints for table `salida`
--
ALTER TABLE `salida`
  ADD CONSTRAINT `fk_salida_movimiento1` FOREIGN KEY (`movimiento_id_movimiento`) REFERENCES `movimiento` (`id_movimiento`),
  ADD CONSTRAINT `fk_salida_pedido_salida1` FOREIGN KEY (`pedido_salida_pedido_id_pedido`) REFERENCES `pedido_salida` (`pedido_id_pedido`);

--
-- Constraints for table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `fk_User_roles` FOREIGN KEY (`roles_Id_rol`) REFERENCES `roles` (`Id_rol`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
